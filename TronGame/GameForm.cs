﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TronGame
{
	class GameForm : Form
	{
		private Model model;

		private Graphics graphics;
		private Label winnerLabel;
		private Label pressSpacebarLabel;
		private Label P1WinCounter;
		private Label P2WinCounter;
		private Color p1Color = Color.FromArgb(0, 254, 17);
		private Color p2Color = Color.FromArgb(0, 13, 249);

		private Timer timer;
		private int interval = 17; //fps = 60 (1000ms / 60 = 16.6)

		public GameForm(Model model)
		{
			//model
			this.model = model;
			model.Update += OnModelUpdate;

			//form setup
			ClientSize = new Size(model.GridWidth, model.GridHeight);
			Text = "Game";
			FormBorderStyle = FormBorderStyle.FixedSingle;
			MaximizeBox = false;
			MinimizeBox = false;
			DoubleBuffered = true;
			BackColor = Color.FromArgb(0, 16, 30);

			winnerLabel = new Label();
			winnerLabel.Font = new Font("Arial", 35, FontStyle.Bold);
			winnerLabel.BackColor = Color.Transparent;
			winnerLabel.Text = "Player 1 Wins!";
			winnerLabel.AutoSize = true;
			winnerLabel.Location = new Point(ClientSize.Width / 2 - winnerLabel.Size.Width * 2,
											 ClientSize.Height / 2 - winnerLabel.Size.Height * 2 - 300);
			winnerLabel.Visible = false;
			Controls.Add(winnerLabel);

			pressSpacebarLabel = new Label();
			pressSpacebarLabel.Font = new Font("Arial", 35, FontStyle.Bold);
			pressSpacebarLabel.BackColor = Color.Transparent;
			pressSpacebarLabel.ForeColor = p1Color;
			pressSpacebarLabel.Text = "Press spacebar to start";
			pressSpacebarLabel.AutoSize = true;
			pressSpacebarLabel.Location = new Point(ClientSize.Width / 2 - (int)(pressSpacebarLabel.Size.Width * 3.3),
													ClientSize.Height / 2 - pressSpacebarLabel.Size.Height * 2);

			P1WinCounter = new Label();
			P1WinCounter.Font = new Font("Arial", 35, FontStyle.Bold);
			P1WinCounter.BackColor = Color.Transparent;
			P1WinCounter.ForeColor = p1Color;
			P1WinCounter.Text = "0";
			P1WinCounter.AutoSize = true;
			P1WinCounter.Location = new Point(30, 30);
			Controls.Add(P1WinCounter);

			P2WinCounter = new Label();
			P2WinCounter.Font = new Font("Arial", 35, FontStyle.Bold);
			P2WinCounter.BackColor = Color.Transparent;
			P2WinCounter.ForeColor = p2Color;
			P2WinCounter.Text = "0";
			P2WinCounter.AutoSize = true;
			P2WinCounter.Location = new Point(ClientSize.Width - 100, 30);
			Controls.Add(P2WinCounter);

			pressSpacebarLabel.Visible = true;
			Controls.Add(pressSpacebarLabel);

			graphics = CreateGraphics();

			Paint += PaintBackground;

			//timer
			timer = new Timer();
			timer.Interval = interval;
			timer.Tick += (sender, e) => model.OnTick();
			timer.Start();
		}

		private void PaintBackground(object sender, PaintEventArgs e)
		{
			var g = e.Graphics;

			var gridPen = new Pen(Color.FromArgb(200, 73, 83, 95), 2);

			int gridSize = ClientSize.Height / 50;
			for (int x = 1; x < ClientSize.Width; ++x)
				if (x % gridSize == 0)
					g.DrawLine(gridPen, x, 0, x, ClientSize.Height);

			for (int y = 1; y < ClientSize.Height; ++y)
				if (y % gridSize == 0)
					g.DrawLine(gridPen, 0, y, ClientSize.Width, y);
		}

		private void OnModelUpdate(List<Point> p1Points, List<Point> p2Points)
		{
			var g = graphics;
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

			if (model.State == 1)
			{
				winnerLabel.Text = "Player 1 Wins!";
				winnerLabel.ForeColor = p1Color;
				winnerLabel.Visible = true;
				pressSpacebarLabel.ForeColor = p1Color;
				pressSpacebarLabel.Visible = true;
				P1WinCounter.Text = model.P1Wins.ToString();
				P2WinCounter.Text = model.P2Wins.ToString();
			}
			else if (model.State == 2)
			{
				winnerLabel.Text = "Player 2 Wins!";
				winnerLabel.ForeColor = p2Color;
				winnerLabel.Visible = true;
				pressSpacebarLabel.ForeColor = p2Color;
				pressSpacebarLabel.Visible = true;
				P1WinCounter.Text = model.P1Wins.ToString();
				P2WinCounter.Text = model.P2Wins.ToString();
			}
			else if (winnerLabel.Visible)
				winnerLabel.Visible = false;

			if (model.State == 0 && pressSpacebarLabel.Visible)
			{
				pressSpacebarLabel.Visible = false;
				Invalidate();
			}

			foreach (var p in p1Points)
				g.FillCircle(new SolidBrush(p1Color), p.X, p.Y, 3);
			foreach (var p in p2Points)
				g.FillCircle(new SolidBrush(p2Color), p.X, p.Y, 3);
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.W:
					model.SetP1Direction(0);
					break;
				case Keys.A:
					model.SetP1Direction(1);
					break;
				case Keys.S:
					model.SetP1Direction(2);
					break;
				case Keys.D:
					model.SetP1Direction(3);
					break;
				case Keys.Up:
					model.SetP2Direction(0);
					break;
				case Keys.Left:
					model.SetP2Direction(1);
					break;
				case Keys.Down:
					model.SetP2Direction(2);
					break;
				case Keys.Right:
					model.SetP2Direction(3);
					break;
				case Keys.Space:
					model.Start();
					break;
			}
		}
	}

	public static class GraphicsExtensions
	{
		public static void FillCircle(this Graphics g, Brush brush,
									  float centerX, float centerY, float radius)
		{
			g.FillEllipse(brush, centerX - radius, centerY - radius,
						  radius + radius, radius + radius);
		}
	}
}
