﻿using System;
using System.Windows.Forms;

namespace TronGame
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
			var model = new Model(960, 720, 10);
			var form = new GameForm(model);
			Application.Run(form);
		}
	}
}
